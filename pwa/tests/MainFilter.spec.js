import { shallowMount } from '@vue/test-utils'
import MainFilter from '@/components/MainFilter.vue'
import axios from 'axios';
import { BFormDatepicker } from 'bootstrap-vue';

const mockHotelList = { data: {
  'hydra:member': [
    { id: 11, name: 'First' },
    { id: 15, name: 'Second' },
  ]
}}

jest.spyOn(axios, 'get').mockResolvedValue(mockHotelList)

describe('MainFilter.vue', () => {
  it('renders correctly', () => {
    const wrapper = shallowMount(MainFilter, {
      mocks: {
        axios
      },
      stubs: {
        'b-form-datepicker': BFormDatepicker,
      },
    })
    expect(wrapper.element).toMatchSnapshot()
  })
})
