import { shallowMount } from '@vue/test-utils'
import DashboardChart from '@/components/DashboardChart.vue'

describe('DashboardChart.vue', () => {
  it('renders correctly', () => {
    const wrapper = shallowMount(DashboardChart, {
      propsData: {
        reviews: [
          { id: 10, score: 34, createdDate: '2020-01' },
          { id: 5, score: 60, createdDate: '2020-02' },
        ]
      }
    })
    expect(wrapper.element).toMatchSnapshot()
  })
})
