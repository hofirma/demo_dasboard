module.exports = {
  runtimeCompiler: true,
  productionSourceMap: false,
  lintOnSave: process.env.NODE_ENV !== 'production',
  devServer: {
    disableHostCheck: true,
    host: 'localhost',
    port: 8080,
    overlay: {
      warnings: true,
      errors: true
    }
  },
  publicPath: '/',
}
