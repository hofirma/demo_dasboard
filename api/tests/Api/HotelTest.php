<?php

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Hotel;

class HotelTest extends ApiTestCase
{
    public function testGetHotels()
    {
        $response = static::createClient()->request('GET', '/hotels');

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains([
            '@context' => '/contexts/Hotel',
            '@type' => 'Hotel',
        ]);
    }
}
