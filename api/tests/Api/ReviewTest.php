<?php

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Review;

class ReviewTest extends ApiTestCase
{
    public function testGetHotels()
    {
        $response = static::createClient()->request('GET', 'review-statistic?hotel=315&createdDate[before]=2021-06-16 23:59:59&createdDate[after]=2020-06-10');

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains([
            '@context' => '/contexts/Review',
            '@type' => 'Review',
        ]);
    }
}
