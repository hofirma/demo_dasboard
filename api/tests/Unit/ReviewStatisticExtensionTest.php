<?php

namespace App\Tests\Unit;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Doctrine\ReviewStatisticExtension;
use App\Entity\Review;
use Doctrine\ORM\QueryBuilder;
use PHPUnit\Framework\TestCase;

class ReviewStatisticExtensionTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBuilder = $this->createMock(QueryBuilder::class);
        $this->queryNameGenerator = $this->createMock(QueryNameGeneratorInterface::class);
    }

    /**
     * @dataProvider testDataProvider
     */
    public function testApplyToCollection(string $from, string $to, string $expression)
    {
        $extension = new ReviewStatisticExtension();
        $this->queryBuilder->expects($this->once())
            ->method('getRootAliases')
            ->willReturn(['o']);

        $this->queryBuilder->expects($this->once())
            ->method('select')
            ->with("COUNT(o.id) as id, ROUND(AVG(o.score), 1) as score, $expression")
            ->willReturn($this->queryBuilder);

        $this->queryBuilder->expects($this->once())
            ->method('groupBy')
            ->with('createdDate')
            ->willReturn($this->queryBuilder);

        $this->queryBuilder->expects($this->once())
            ->method('orderBy')
            ->with('createdDate')
            ->willReturn($this->queryBuilder);

        $extension->applyToCollection(
            $this->queryBuilder,
            $this->queryNameGenerator,
            Review::class,
            Review::COLLECTION_OPERATION_GET_STATISTIC,
            ['filters' => ['createdDate' => ['after' => $from, 'before' => $to]]]
        );
    }

    public function testDataProvider(): array
    {
        return [
            ['2020-12-25', '2021-01-12', 'DATE(o.createdDate) as createdDate'],
            ['2020-12-25', '2021-02-01', "TO_CHAR(o.createdDate, 'IYYY-IW') as createdDate"],
            ['2020-12-25', '2021-05-25', "TO_CHAR(o.createdDate, 'YYYY-MM') as createdDate"],
        ];
    }
}
