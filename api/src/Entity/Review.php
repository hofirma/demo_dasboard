<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiFilter(SearchFilter::class, properties: ['hotel' => 'exact'])]
#[ApiFilter(DateFilter::class, properties: ['createdDate'])]
#[ApiResource(
    collectionOperations: [
        Review::COLLECTION_OPERATION_GET_STATISTIC => [
            'method' => 'GET',
            'path' => 'review-statistic',
            'pagination_enabled' => false,
            'properties' => ['createdDate'],
        ],
    ],
    mercure: true,
)]
#[ORM\Entity]
class Review
{
    public const COLLECTION_OPERATION_GET_STATISTIC = 'get_statistic';

    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    private ?int $id = null;

    #[ORM\Column(type: 'integer')]
    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(value: 0)]
    #[Assert\LessThanOrEqual(value: 100)]
    public int $score = 0;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank]
    public string $comment = '';

    #[ORM\Column(name: 'created_date', type: 'datetime_immutable', nullable: false)]
    #[Assert\NotBlank]
    public \DateTimeImmutable $createdDate;

    #[ORM\ManyToOne(targetEntity: 'Hotel')]
    #[ORM\JoinColumn(name: 'hotel_id', referencedColumnName: 'id')]
    public ?Hotel $hotel = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedDate(): \DateTimeImmutable
    {
        return $this->createdDate;
    }

    public function setCreatedDate(\DateTime|\DateTimeImmutable $createdDate): self
    {
        if ($createdDate instanceof \DateTime) {
            $createdDate = \DateTimeImmutable::createFromMutable($createdDate);
        }
        $this->createdDate = $createdDate;

        return $this;
    }
}
