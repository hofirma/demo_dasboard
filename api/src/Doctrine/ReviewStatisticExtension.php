<?php

namespace App\Doctrine;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Review;
use Doctrine\ORM\QueryBuilder;

class ReviewStatisticExtension implements QueryCollectionExtensionInterface
{
    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
    ) {
        // there is a bug in QueryCollectionExtensionInterface. There should be 5 params
        $args = func_get_args();
        if (count($args) < 5) {
            return;
        }
        $context = $args[4];
        if ($resourceClass === Review::class && $operationName === Review::COLLECTION_OPERATION_GET_STATISTIC) {
            $createdFrom = $context['filters']['createdDate']['after'] ?? false;
            $createdTo = $context['filters']['createdDate']['before'] ?? false;
            if (!$createdFrom || !$createdTo) {
                throw new \LogicException('Invalid value of createdDate');
            }

            $rootAlias = $queryBuilder->getRootAliases()[0];
            $selectDateExpression = $this->selectDateExpression($rootAlias, $createdFrom, $createdTo);
            $queryBuilder
                ->select("COUNT($rootAlias.id) as id, ROUND(AVG($rootAlias.score), 1) as score, $selectDateExpression")
                ->groupBy('createdDate')
                ->orderBy('createdDate')
            ;
        }
    }

    private function selectDateExpression(string $rootAlias, string $createdFrom, string $createdTo): string
    {
        $date1 = new \DateTimeImmutable($createdFrom);
        $date2 = new \DateTimeImmutable($createdTo);
        $interval = $date1->diff($date2)->days;
        return match(true) {
            $interval <= 29 => "DATE($rootAlias.createdDate) as createdDate",
            $interval <= 89 => "TO_CHAR($rootAlias.createdDate, 'IYYY-IW') as createdDate",
            default => "TO_CHAR($rootAlias.createdDate, 'YYYY-MM') as createdDate",
        };
    }
}
