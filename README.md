The demo based on the API Platform.
<h1 align="center"><a href="https://api-platform.com"><img src="https://api-platform.com/logo-250x250.png" alt="API Platform"></a></h1>

API Platform is a next-generation web framework designed to easily create API-first projects without compromising extensibility
and flexibility.

[Read the official API Platform "Getting Started" guide](https://api-platform.com/docs/distribution).

Created by [Kévin Dunglas](https://dunglas.fr). Commercial support available at [Les-Tilleuls.coop](https://les-tilleuls.coop).

<a href="https://ibb.co/m57wJvH"><img src="https://i.ibb.co/m57wJvH/Screenshot-2022-06-13-at-08-13-46.png" alt="Screenshot-2022-06-13-at-08-13-46" border="0"></a>

### Requirements:
- Mac or Linux
- Docker compose.
- npm v8

### Build Setup

``` bash
docker-compose up -d

# go to php container
docker exec -ti c_all_demo_php_1 sh

# apply migrations
bin/console doctrine:migrations:migrate

# load fixtures
php -d memory_limit=2G bin/console hautelook:fixtures:load

# unit test
php bin/phpunit tests/Unit/ReviewStatisticExtensionTest.php
```

``` bash
cd pwa

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run serve

# FE tests
npm run test
```

Go to the https://localhost/docs to see Swagger API documentation (click proceed unsafe) 
